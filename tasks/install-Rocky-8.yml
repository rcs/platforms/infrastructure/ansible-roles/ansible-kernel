---
- name: "Configure custom kernel repository if required"
  when: kernel_manage_custom_repo | bool
  block:
    - name: "Install custom kernel yum repo"
      yum_repository:
        name: "{{ kernel_custom_repo_name }}"
        description: "{{ kernel_custom_repo_description }}"
        baseurl: "{{ kernel_custom_repo_baseurl }}"
        gpgcheck: "{{ kernel_custom_repo_gpgcheck | default('no') }}"
        gpgkey: "{{ kernel_custom_repo_gpgkey | default(omit) }}"
        enabled: "{{ kernel_custom_repo_enabled | default('no') }}"
      register: _kernel_custom_repo

    - name: "Install custom kernel yum repo gpgkey"
      rpm_key:
        state: present
        key: "{{ kernel_custom_repo_gpgkey }}"
        fingerprint: "{{ kernel_custom_repo_gpgkey_fingerprint | default(omit) }}"
      when: kernel_custom_repo_gpgkey is defined

- name: "Clean yum cache if repository updated"
  command: yum clean all
  changed_when: false
  args:
    warn: false

- name: "Get available kernel versions with DNF"
  command: dnf list --showduplicates kernel
  changed_when: false
  args:
    warn: false
  when:
    - ansible_os_family == 'Rocky'
    - ansible_distribution_major_version != "7"
  register: dnf_kernel_list

- name: "Set target DNF kernel version"
  set_fact: _kernel="{{ dnf_kernel_list |
                     rhel_kernel(kernel_version, ansible_kernel) }}"
  when: dnf_kernel_list is not skipped

- name: "Ensure kernel packages versions with DNF"
  dnf:
    name: "{{ item.name }}-{{ _kernel }}"
    enablerepo: "{{ dnf_kernel_list |
                 rhel_repo(kernel_version, ansible_kernel) }}"
    allow_downgrade: true
  when:
    - ansible_os_family == 'Rocky'
    - ansible_distribution_major_version != "7"
    - item.when
  with_items:
    - name: kernel
      when: true
    - name: kernel-devel
      when: "{{ install_kernel_headers | bool }}"
    - name: kernel-headers
      when: "{{ install_kernel_headers | bool }}"

